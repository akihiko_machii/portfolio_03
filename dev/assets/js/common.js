'use strict';

(function($) {
  var config = function config() {
    //ポップアップリンクに置換
    $('.commonPop').easyPop();
    //アンカーリンクをスムージング
    $('a[href^="#"]').smoothScroll();
    // 判別用
    var _ua = Fourdigit.set()._ua,
      _browser = Fourdigit.set()._browser,
      _breakP = Fourdigit.set()._breakP,
      _winSize = Fourdigit.set()._winSize;
    // ブラウザを判別し、bodyにそのブラウザ名のクラスを付与
    for (var key in _browser) {
      if (_browser.hasOwnProperty(key)) {
        if (_browser[key] == true) {
          $('body').addClass(key);
        }
      }
    }
  };

  function breakPoint() {
    var point = {
      PC: 1024,
      SP: 736
    };
    var ww = window.innerWidth;
    var PC, SP;

    if (ww > point.PC) {
      PC = true;
    } else {
      PC = false;
    }

    if (ww < point.SP) {
      SP = true;
    } else {
      SP = false;
    }

    return { PC: PC, SP: SP };
  }

  $(function() {
    /**
     * 共通系処理
     * @description
     * サイト共通で機能させる処理はここに書きます。
     */
    config();

    // 共通変数
    var $html = $('html'),
      $body = $('body'),
      $gHdr = $('#gHeader'),
      $gNav = $('#gNav'),
      $pNav = $('#pNav'),
      $spNav = $('#spNav'),
      $spMenu = $('#spMenu'),
      $gFtr = $('#gFooter'),
      $fNav = $('#fNav'),
      $pagetop = $('#pagetop');

    /**
     * VIEWPORT 切り替え
     * @description
     * TBとSPの場合で、それぞれviewportの値を切り替えます。
     */
    function updateMetaViewport() {
      var viewportContent;
      if (_ua.SP) {
        viewportContent = 'width=device-width,initial-scale=1.0,maximum-scale=1.5,user-scalable=yes';
      } else if (_ua.TB) {
        viewportContent = 'width=1200px';
      }
      document.querySelector("meta[name='viewport']").setAttribute('content', viewportContent);
    }
    if (_ua.SP || _ua.TB) {
      window.addEventListener('load', updateMetaViewport, false);
    }
    /**
     * android tel設定
     * @description
     * androidで電話がかけられないバグ用の記述です。
     */
    if (_ua.SP) {
      $('a[href^=tel]').on('click', function() {
        location.href = $(this).attr('href');
        return false;
      });
    }
    /**
     * PAGE TOP
     * @description
     */
    $pagetop.on('click', function() {
      $body.animate(
        {
          scrollTop: 0
        },
        500,
        'linear'
      );
      return false;
    });
    /**
     * SP NAVI
     * @description
     */
    var spNavFlag = false;
    if (_breakP.SP) {
      $spMenu.on('click', function() {
        if (spNavFlag) {
          $spMenu.removeClass('is-active');
          $gNav.fadeOut().removeClass('is-active');
          spNavFlag = false;
        } else {
          $spMenu.addClass('is-active');
          $gNav.fadeIn().addClass('is-active');
          spNavFlag = true;
        }
      });
    }
    // webfont setting
    (function() {
      WebFont.load({
        google: {
          families: ['Source+Serif+Pro:400,600,700']
        },
        active: function active() {
          sessionStorage.fonts = true;
        }
      });
    })();

    function aosSetting() {
      var wh = window.outerHeight;
      AOS.init({
        offset: wh / 5, // offset (in px) from the original trigger point
        duration: 0, // values from 0 to 3000, with step 50ms
        easing: 'ease', // default easing for AOS animations
        once: true, // whether animation should happen only once - while scrolling down
        mirror: false, // whether elements should animate out while scrolling past them
        anchorPlacement: 'top-bottom' // defines which position of the element regarding to window should trigger the animation
      });
    }

    /*
     * textAnimation(Split)
     * */
    /*
     * 第1引数：対象要素
     * 第2引数：spanのclass名
     * */
    function spanSplit(el, className) {
      /*要素の指定*/
      var $el = $(el);
      $el.each(function() {
        var el = this;
        /*指定した要素からnodeを取り出し*/
        $(el)
          .contents()
          .each(function(_, node) {
            /*nodeを一旦削除*/
            node.parentNode.removeChild(node);
            switch (node.nodeType) {
              /*nodeがテキストだった場合*/
              case Node.TEXT_NODE:
                /*テキストを分割*/
                var text_split = splitText(node);
                /*スパンで囲む*/
                $.each(text_split, function() {
                  var random = Math.floor(Math.random() * (5 - 1) + 1);
                  var transClass;
                  switch (random) {
                    case 1:
                      transClass = 'type-right';
                      break;
                    case 2:
                      transClass = 'type-left';
                      break;
                    case 3:
                      transClass = 'type-top';
                      break;
                    case 4:
                      transClass = 'type-bottom';
                      break;
                  }
                  $(el).append('<span class="' + className + ' ' + transClass + '">' + this + '</span>');
                });
                break;
              default:
                /*spanの中身にも適用*/
                if (node.childNodes.length > 0) {
                  /*テキストを分割*/
                  var text_split = splitText(node);
                  var e = '';
                  /*タグを分割するために中身を,*/
                  node.innerHTML = ',';
                  /*wrapのためにタグを分割*/
                  var wrap = node.outerHTML.split(',');
                  /*スパンで囲む*/
                  $.each(text_split, function() {
                    var random = Math.floor(Math.random() * (5 - 1) + 1);
                    var transClass;
                    switch (random) {
                      case 1:
                        transClass = 'type-right';
                        break;
                      case 2:
                        transClass = 'type-left';
                        break;
                      case 3:
                        transClass = 'type-top';
                        break;
                      case 4:
                        transClass = 'type-bottom';
                        break;
                    }
                    e += '<span class="' + className + ' ' + transClass + '">' + this + '</span>';
                  });
                  /*タグをまとめて囲む*/
                  e = wrap[0] + e + wrap[1];
                  $(el).append(e);
                } else {
                  /*nodeはそのまま*/
                  $(el).append(node);
                }
            }
          });
      });
      function splitText(node) {
        /*テキストからスペースを削除*/
        var text = node.textContent.trim();
        /*一文字ずつスプリット*/
        var text_split = text.split('');

        return text_split;
      }
    }

    // インタラクション設定
    function originalFade() {
      // originalFadeの直下要素にクラスを追加
      var $originalFade = document.querySelectorAll('[data-aos="originalFade"]');
      for (var i = 0; i < $originalFade.length; i++) {
        var $children = $originalFade[i].children;
        $($children).each(function() {
          $(this).addClass('originalFade__children--option');
          $(this).addClass('originalFade__children--out');
        });
      }

      // observerを新規宣言
      var observer = new MutationObserver(function(MutationRecords, MutationObserver) {
        // MutationRecordsに監視してる要素が入ってる
        MutationRecords.forEach(function(mutation) {
          // mutation.targetで監視要素が拾える
          var $this = $(mutation.target);
          // 監視している要素にaos-animateが付いていたら実行
          if ($this.hasClass('aos-animate')) {
            $this.children().each(function(i) {
              var count = i;
              var $self = $(this);
              setTimeout(function() {
                $self.removeClass('originalFade__children--out');
              }, 180 * count);
            });
          }
        });
      });
      // observerのオプション
      var observer_option = {
        // 属性を監視
        attributes: true,
        // classの変動のみ監視
        attributeFilter: ['class']
      };
      // 監視させたい要素を指定
      for (var i = 0; i < $originalFade.length; i++) {
        observer.observe($originalFade[i], observer_option);
      }
    }

    // topleadの画像がはみ出した分だけマージンをつける
    function topLeadMargin() {
      var $block = document.querySelectorAll('.c-topLead__block'),
        $toplead = document.querySelectorAll('.c-topLead'),
        $flex = document.querySelectorAll('.c-topLead__flex'),
        $photo = document.querySelectorAll('.c-topLead__photo'),
        $lead = document.querySelectorAll('.c-topLead__lead');
      if ($toplead.length > 0) {
        for (var i = 0; i < $block.length; i++) {
          var blockHeight = $block[i].clientHeight;
          var flexTop = $flex[i].offsetTop;
          var photoHeight = $photo[i].clientHeight;
          var leadHeight = $lead[i].clientHeight;
          var photoInnerHeight = blockHeight - flexTop;
          var photoOuterHeight = photoHeight - photoInnerHeight;
          var marginHeight = photoOuterHeight - leadHeight;
          if (marginHeight < 0) {
            marginHeight = 0;
          }
          if (breakPoint().SP) {
            var marginTop = Number(
              $($photo)
                .eq(i)
                .css('margin-top')
                .slice(0, -2)
            );
            marginHeight = photoHeight + marginTop;
            $lead[i].style.marginBottom = 0 + 'px';
            $block[i].style.marginBottom = marginHeight + 'px';
          } else {
            $lead[i].style.marginBottom = marginHeight + 'px';
            $block[i].style.marginBottom = 0 + 'px';
          }
        }
      }
    }

    function copyMenu() {
      var $header = document.querySelector('.c-header');
      var $nav = document.querySelector('.c-nav__wrap');
      var clone = $nav.cloneNode(true);

      $($header).prepend(clone);
      // $($header)
      //   .find('.c-nav__nav')
      //   .remove();
    }
    function menuHeight() {
      var $target1 = document.querySelector('.c-header__section');
      var $target2 = document.querySelector('.c-nav__wrap');
      var $targetOuter = document.querySelector('.c-header');
      var TARGET_HEIGHT = $target1.clientHeight + $target2.clientHeight;
      var WINDOW_HEIGHT = window.innerHeight;
      if (TARGET_HEIGHT > WINDOW_HEIGHT) {
        $targetOuter.classList.add('c-header--start');
      } else {
        $targetOuter.classList.remove('c-header--start');
      }
      var $header = document.querySelector('.c-header');
      window.addEventListener('scroll', _handleScroll, false);
      function _handleScroll() {
        $header.style.left = -window.scrollX + 'px';
      }
    }
    // menu
    function toggleMenu() {
      var $hamburger = document.querySelector('.c-hamburger');
      var $menu = document.querySelector('.c-header');
      var $container = document.querySelector('#container');
      var isShow = false;
      function clickAction(isShow) {
        $hamburger.classList.toggle('c-hamburger--on');
        $menu.classList.toggle('c-header--on');
        $container.classList.toggle('on');
        function menuAnimation() {
          var tl = new TimelineMax();
          tl.set('.c-header__animation', { opacity: 0, y: '50%' }).staggerTo(
            '.c-header__animation',
            1,
            {
              delay: 0.4,
              opacity: 1,
              y: '0%',
              ease: Power1.ease,
              onStart: function onStart() {
                $hamburger.classList.add('a-animationNow');
                $menu.classList.add('a-animationNow');
              },
              onComplete: function onComplete() {
                $hamburger.classList.remove('a-animationNow');
                $menu.classList.remove('a-animationNow');
              }
            },
            0.2
          );
          tl.restart();
        }
        isShow = !isShow;
        if (isShow && breakPoint().PC) {
          menuAnimation();
        }
        return isShow;
      }
      $hamburger.addEventListener('click', function() {
        isShow = clickAction(isShow);
      });
      // $(document).click(function(event) {
      //   if (
      //     !$(event.target).closest('.o-header__inner').length &&
      //     !$(event.target).closest('.m-hamburger').length &&
      //     isShow
      //   ) {
      //     isShow = clickAction(isShow);
      //   }
      // });
      // $('.o-header__link').on('click', function() {
      //   isShow = clickAction(isShow);
      // });
    }

    window.addEventListener('load', function() {
      copyMenu();
      spanSplit('[data-aos="aos-headline"]', 'type');
      aosSetting();
      topLeadMargin();
      originalFade();
      toggleMenu();
      menuHeight();
    });
    window.addEventListener('resize', function() {
      topLeadMargin();
      menuHeight();
    });

    /**
     * 各ページ固有の処理
     * 基本的にローカルにJSは置かずにcommon内で完結させる。
     */
    switch ($('.page').attr('id')) {
      case 'index':
        // $(window).on('load resize', function() {
        //   var wW = $(window).width();
        //   var ratio = 360 / 1600;
        //   $('.p-mainVisual__inner').css({
        //     right: wW * ratio + 'px'
        //   });
        // });

        break;

      case 'plan':
        (function() {
          // planアンカーリンクのon判断
          var $plan = document.querySelectorAll('.p-plan');
          var $tab = document.querySelectorAll('.p-planTab__item');
          for (var i = 0; i < $plan.length; i++) {
            var id = $plan[i].getAttribute('id');
            var $anchor = $plan[i].querySelectorAll('.p-planAnchor__link');
            for (var n = 0; n < $anchor.length; n++) {
              var href = $anchor[n].getAttribute('href').replace('#', '');
              if (id === href) {
                $anchor[n].classList.add('p-planAnchor__link--on');
              }
            }
          }

          // planTab
          $($tab).on('click', function() {
            var $parent = this.parentNode.parentNode;
            var $item = $parent.querySelectorAll('.p-planTab__item');
            var $madori = $parent.querySelectorAll('.p-planBlock__madori');
            var index = $($item).index($(this));
            $($item).removeClass('p-planTab__item--on');
            $($madori).removeClass('p-planBlock__madori--on');
            $item[index].classList.add('p-planTab__item--on');
            $madori[index].classList.add('p-planBlock__madori--on');
          });
        })();
        break;

      case 'accessLocation':
        $(function() {
          $('.p-trainAccess__map').on('scroll touchmove', function() {
            $(this).addClass('iconNone');
          });
          $('.p-trainAccess__map').on('touchend', function() {
            setTimeout(function() {
              $('.p-trainAccess__map').removeClass('iconNone');
            }, 3000);
          });

          $('#image1').imgViewer2();
        });

        break;
      default:
        break;
    }
  });
})(jQuery);
