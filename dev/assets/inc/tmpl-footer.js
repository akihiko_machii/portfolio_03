/**
 * Template Footer.
 */

/**
 * '_template', index : null
 * 'plan', index : 1
 * 'accesslocation', index : 2
 * 'map', index : 2
 * 'outline', index : 3
 */
var tf = {};
tf.onChoice = null;
tf.onArray = {};
tf.init = function() {
  for (var i = 0; i < incNav.onArray.length; i++) {
    if (tf.onChoice == null) {
      if (location.href.indexOf(incNav.onArray[i].dir) > -1) tf.onChoice = incNav.onArray[i];
    }
    tf.onArray[incNav.onArray[i].dir] = incNav.onArray[i];
  }
  if (tf.onChoice == null) tf.onChoice = { dir: '', index: 0, layer: 0, next: '', back: '' };
  window.document.write(tf.createTag());
};
tf.createTag = function() {
  var result = '';
  var layer = '';
  for (var i = 0; i < tf.onChoice.layer; i++) {
    layer += '../';
  }

  // href="' + layer + ' // パス（../）の変数
  // if(tf.onChoice.index == 0) result += 'on '; それぞれ固有の処理をさせたい時（番号はindexに基づく
  // if(tf.onArray._template.off) result += 'off '; off処理
  // next back
  // if(tf.onChoice.back != '') result += '<li class="back"><a href="../' + tf.onChoice.back + '/index.html">BACK</a></li>';
  // if(tf.onChoice.next != '') result += '<li class="next"><a href="../' + tf.onChoice.next + '/index.html">NEXT</a></li>';

  result += '<footer id="gFooter">';
  result += '<section class="c-information imageCap">';
  result += '<div class="c-information__content" data-aos="aos-fadeUp">';
  result += '<h4 class="c-information__title">INFORMATION</h4>';
  result += '<p class="c-information__text c-information__text--01">';
  result += '〈<span class="size--01">2019</span>年<span class="size--01">7</span>月中旬 モデルハウスオープン予定〉';
  result += '</p>';
  result += '<p class="c-information__text c-information__text--02">資料請求受付中</p>';
  result += '<p class="c-information__text c-information__text--03">資料は6月下旬頃発送予定です。</p>';
  result += '</div>';
  result += '<p class="caption ar color-white">哲学堂公園（約260m／徒歩4分）</p>';
  result += '</section>';
  result += '<section class="c-contact">';
  result += '<div class="wrap">';
  result += '<div class="c-btnWrap c-btnWrap--contact">';
  result +=
    '<a class="a-btn a-btn--red" href="#"><span class="a-btn__text">資料請求</span></a>';
  result += '</div>';
  result += '<div class="c-contact__flex">';
  result += '<div class="c-contact__left">';
  result += '<div class="c-contact__left__inner" data-aos="aos-fadeUp">';
  result += '<p class="size--01">お問い合わせ</p>';
  result += '<p class="size--02">「デュオアベニュー中野哲学堂公園」販売準備室</p>';
  result += '<p class="c-contact__tel"><a href="" class="c-contact__telLink">{$TEL}</a></p>';
  result += '</div>';
  result += '</div>';
  result += '<div class="c-contact__line"></div>';
  result += '<div class="c-contact__right" data-aos="aos-fadeUp">';
  result += '営業時間 / 平日 11:00～17:00<br />';
  result += '定休日 / 土日祝日';
  result += '</div>';
  result += '</div>';
  result += '</div>';
  result += '</section>';
  result += '<section class="c-footerCont">';
  result += '<div class="wrap">';
  result += '<div class="c-footerCont__flex">';
  result += '<div class="c-footerCont__left">';
  result += '<p class="c-footerCont__logo">';
  result +=
    '<img class="pc" src="' +
    layer +
    'assets/imgs/footer-logo.svg" alt="DUO AVENUE Nakano Tetsugakudokoen デュオアベニュー中野哲学堂公園" />';
  result +=
    '<img class="sp" src="' +
    layer +
    'assets/imgs/sp/logo.svg" alt="DUO AVENUE Nakano Tetsugakudokoen デュオアベニュー中野哲学堂公園" />';
  result += '</p>';
  result += '<div class="c-footerCont__copy sp">';
  result += '<img class="pc" src="' + layer + 'assets/imgs/footer-copy.svg" alt="都心と森を､ 人生に結ぶ邸。" />';
  result += '<img class="sp" src="' + layer + 'assets/imgs/sp/footer-copy.svg" alt="都心と森を､ 人生に結ぶ邸。" />';
  result += '</div>';
  result += '<nav class="c-nav__nav">';
  result += '<ul class="c-nav__ul">';
  // TOP
  result += '<li class="c-nav__li ';
  // if (th.onChoice.index == 0) result += 'on ';
  result += '"><a class="c-nav__link" href="' + layer + '">TOP</a></li>';
  // PLAN
  result += '<li class="c-nav__li ';
  // if (th.onChoice.index == 1) result += 'on ';
  // if (th.onArray.plan.new) result += 'new ';
  // if (th.onArray.plan.off) result += 'off ';
  result += '"><a class="c-nav__link" href="' + layer + 'plan/">PLAN</a></li>';
  // ACCESS LOCATION
  result += '<li class="c-nav__li ';
  // if (th.onChoice.index == 2) result += 'on ';
  // if (th.onArray.accesslocation.new) result += 'new ';
  // if (th.onArray.accesslocation.off) result += 'off ';
  result += '"><a class="c-nav__link" href="' + layer + 'accesslocation/">ACCESS & LOCATION</a></li>';
  // MAP
  result += '<li class="c-nav__li ';
  // if (th.onChoice.index == 98) result += 'on ';
  // if (th.onArray.map.new) result += 'new ';
  // if (th.onArray.map.off) result += 'off ';
  result += '"><a class="c-nav__link" href="' + layer + 'map/">MAP</a></li>';
  // OUTLINE
  result += '<li class="c-nav__li ';
  // if (th.onChoice.index == 99) result += 'on ';
  // if (th.onArray.outline.new) result += 'new ';
  // if (th.onArray.outline.off) result += 'off ';
  result +=
    '"><a class="c-nav__link" href="#">OUTLINE</a></li>';
  result += '</ul>';
  result += '</nav>';
  result += '<div class="c-footerCont__textBlock">';
  result += '<p class="c-footerCont__text">';
  result +=
    '西武新宿線「新井薬師前」駅徒歩11分、再開発によって魅力を増した「中野」駅までバス6分。中野区松が丘2丁目、分譲戸建プロジェクト、「デュオアベニュー中野哲学堂公園」全15邸誕生。中野区で新築戸建をお探しの方は「デュオアベニュー中野哲学堂公園」をぜひご検討くださいませ。';
  result += '</p>';
  result += '<p class="c-footerCont__caption">';
  // top
  if (th.onChoice.index === 0)
    result +=
      '※掲載の街並み完成予想CGは計画中の図面を元に描いたもので、外観・外構・色調等は実際とは異なる場合があります。なお、外観形状の細部・各種機器・配管及び電柱・架線・周辺景観は省略または簡略化しております。行政指導や改良のため、変更となる場合があります。植栽は特定の季節を想定したものではなく、今後樹種・位置・大きさ等変更となる場合があります。また、竣工時ではなく、生育後を想定して描いております。';
  // plan
  if (th.onChoice.index === 1) result += '';
  // accesslocation
  if (th.onChoice.index === 2)
    result +=
      '※掲載の所要時間は日中平常時、（　）内は通勤時のもので、時間帯により異なります。また、乗り換え、待ち時間は含まれておりません。<br>※掲載の情報は2018年6月時点のものです。<br>※徒歩分数は80mを1分として算出（端数切上）したものです。<br>※掲載の写真は2019年4月に撮影したものです。<br>※掲載の航空写真は2019年4月に撮影したもので、CG処理を施しています。';
  // map
  if (th.onChoice.index === 98) result += '';
  // outline
  if (th.onChoice.index === 99) result += '';
  result += '</p>';
  result += '</div>';
  result += '</div>';
  result += '<div class="c-footerCont__right pc">';
  result += '<div class="c-footerCont__copy">';
  result += '<img src="' + layer + 'assets/imgs/footer-copy.svg" alt="その丘には、森と都心の気配がある" />';
  result += '</div>';
  result += '<p class="c-footerCont__name">DUO AVENUE Nakano Tetsugakudokoen</p>';
  result += '</div>';
  result += '</div>';
  result += '</div>';
  result += '</section>';
  result += '</footer>';

  return result;
};
tf.init();
