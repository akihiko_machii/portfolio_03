/**
 * 共通化nav情報
 */
var incNav = incNav || {};
incNav.onArray = [
  // 上が優先。
  { dir: '_template', index: null, layer: 1, new: false, off: false, next: '', back: '' },
  { dir: 'plan', index: 1, layer: 1, new: false, off: false, next: '', back: '' },
  { dir: 'accesslocation', index: 2, layer: 1, new: false, off: false, next: '', back: '' },
  { dir: 'map', index: 98, layer: 1, new: false, off: false, next: '', back: '' },
  { dir: 'outline', index: 99, layer: 1, new: false, off: false, next: '', back: '' }
];

/**
 * '_template', index : null
 * 'plan', index : 1
 * 'accesslocation', index : 2
 * 'map', index : 2
 * 'outline', index : 3
 */

/**
 * Template Header.
 */
var th = {};
th.onChoice = null;
th.onArray = {};
var siteAddress = location.href;
if (siteAddress.indexOf('?')) {
  var siteArray = siteAddress.split('?');
  siteAddress = siteArray[0];
}
th.init = function() {
  for (var i = 0; i < incNav.onArray.length; i++) {
    if (th.onChoice == null) {
      if (location.href.indexOf(incNav.onArray[i].dir) > -1) th.onChoice = incNav.onArray[i];
    }
    th.onArray[incNav.onArray[i].dir] = incNav.onArray[i];
  }
  if (th.onChoice == null) th.onChoice = { dir: '', index: 0, layer: 0, next: '', back: '' };
  window.document.write(th.createTag());
};
th.createTag = function() {
  var result = '';
  var layer = '';
  for (var i = 0; i < th.onChoice.layer; i++) {
    layer += '../';
  }

  // href="' + layer + ' // パス（../）の変数
  // if(th.onChoice.index == 1) result += 'on '; それぞれ固有の処理をさせたい時（番号はindexに基づく
  // if(th.onArray._template.new) result += 'new '; NEWマーク
  // if(th.onArray._template.off) result += 'off '; off処理

  result += '<div class="c-hamburger" id="#js-hamburger">';
  result += '<i class="c-hamburger__line c-hamburger__line--01"></i>';
  result += '<i class="c-hamburger__line c-hamburger__line--02"></i>';
  result += '<i class="c-hamburger__line c-hamburger__line--03"></i>';
  result += '<p class="c-hamburger__text">MENU</p>';
  result += '</div>';
  result += '<header id="gHeader" class="c-header">';
  result += '<section class="c-header__section">';
  result += '<div class="bigWrap">';
  result += '<nav class="c-header__rows">';
  result += '<a href="' + layer + '" class="sp c-header__rowClm c-header__animation">';
  result += '<p class="c-header__rowText">TOP</p>';
  result += '</a>';
  result += '<a href="' + layer + 'plan/" class="c-header__rowClm c-header__animation">';
  result += '<div class="c-header__photo"><img src="' + layer + 'assets/imgs/img-menu-plan.jpg" alt="" /></div>';
  result += '<p class="c-header__rowText">PLAN</p>';
  result += '</a>';
  result += '<a href="' + layer + 'accesslocation/" class="c-header__rowClm c-header__animation">';
  result +=
    '<div class="c-header__photo"><img src="' + layer + 'assets/imgs/img-menu-access-location.jpg" alt="" /></div>';
  result += '<p class="c-header__rowText">ACCESS & LOCATION</p>';
  result += '</a>';
  result += '<a href="' + layer + 'map/" class="c-header__rowClm c-header__animation">';
  result += '<div class="c-header__photo"><img src="' + layer + 'assets/imgs/img-menu-map.jpg" alt="" /></div>';
  result += '<p class="c-header__rowText">MAP</p>';
  result += '</a>';
  result +=
    '<a href="#" class="c-header__rowClm c-header__animation sp">';
  result += '<p class="c-header__rowText">OUTLINE</p>';
  result += '</a>';
  result += '</nav>';
  result += '<div class="c-header__cv c-btnWrap c-btnWrap--multi c-header__animation sp">';
  result +=
    '<a href="#" class="a-btn a-btn--cv">';
  result += '<span class="a-btn__text">資料請求</span>';
  result += '</a>';
  // result += '<a href="https://www.hoo-sumai.com/form/index.php?gt=reserve&id=322" class="a-btn a-btn--cv">';
  // result += '<span class="a-btn__text">来場予約</span>';
  // result += '</a>';
  result += '</div>';
  result += '</div>';
  result += '</section>';
  result += '</header>';
  result += '<section class="c-nav">';
  result += '<div class="c-nav__wrap wrap">';
  result += '<div class="c-nav__menu">';
  result += '<h1 class="headline headline-01 ';
  if (th.onChoice.index == 0) result += 'on ';
  result += '" id="logo">';
  result += '<a href="' + layer + '">';
  result += '<img class="pc" src="' + layer + 'assets/imgs/logo.svg" alt="中野区の新築一戸建て「デュオアベニュー中野哲学堂公園」【公式HP】" />';
  result += '<img class="sp" src="' + layer + 'assets/imgs/sp/logo.svg" alt="中野区の新築一戸建て「デュオアベニュー中野哲学堂公園」【公式HP】" />';
  result += '</a>';
  result += '</h1>';
  result += '<nav id="pNav" class="c-nav__nav">';
  result += '<ul class="c-nav__ul">';
  // TOP
  result += '<li class="c-nav__li ';
  if (th.onChoice.index == 0) result += '';
  result += '"><a class="c-nav__link" href="' + layer + '">TOP</a></li>';
  // PLAN
  result += '<li class="c-nav__li ';
  if (th.onChoice.index == 1) result += 'on ';
  if (th.onArray.plan.new) result += 'new ';
  if (th.onArray.plan.off) result += 'off ';
  result += '"><a class="c-nav__link" href="' + layer + 'plan/">PLAN</a></li>';
  // ACCESS LOCATION
  result += '<li class="c-nav__li ';
  if (th.onChoice.index == 2) result += 'on ';
  if (th.onArray.accesslocation.new) result += 'new ';
  if (th.onArray.accesslocation.off) result += 'off ';
  result += '"><a class="c-nav__link" href="' + layer + 'accesslocation/">ACCESS & LOCATION</a></li>';
  // MAP
  result += '<li class="c-nav__li ';
  if (th.onChoice.index == 98) result += 'on ';
  if (th.onArray.map.new) result += 'new ';
  if (th.onArray.map.off) result += 'off ';
  result += '"><a class="c-nav__link" href="' + layer + 'map/">MAP</a></li>';
  // OUTLINE
  result += '<li class="c-nav__li ';
  if (th.onChoice.index == 99) result += 'on ';
  if (th.onArray.outline.new) result += 'new ';
  if (th.onArray.outline.off) result += 'off ';
  result +=
    '"><a class="c-nav__link" href="#">OUTLINE</a></li>';
  result += '</ul>';
  result += '</nav>';
  result += '</div>';
  result += '<div class="c-nav__cv">';
  result +=
    '<a href="#" class="a-btn a-btn--cv">';
  result += '<span class="a-btn__text">資料請求</span>';
  result += '</a>';
  result += '<a href="' + layer + 'map/" class="a-btn a-btn--map">';
  result += '<span class="a-btn__text">MAP</span>';
  result += '</a>';
  result += '</div>';
  result += '</div>';
  result += '</section>';
  return result;
};
th.init();
